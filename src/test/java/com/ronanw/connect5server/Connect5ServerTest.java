package com.ronanw.connect5server;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.lang.reflect.Field;
import java.net.InetSocketAddress;
import java.net.URI;
import java.util.List;
import java.util.Map;

import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpPrincipal;
import com.sun.net.httpserver.HttpServer;
import com.sun.net.httpserver.Authenticator;
import com.sun.net.httpserver.Filter;
import com.sun.net.httpserver.Headers;

public class Connect5ServerTest extends TestCase {
    private Connect5Server connect5Server;
    final int PLAYER_X = 0, PLAYER_O = 1;

    @Override
    protected void setUp() throws Exception {
        connect5Server = new Connect5Server(true);
    }

    @Override
    protected void tearDown() throws Exception {
        Field server = connect5Server.getClass().getDeclaredField("server");
        server.setAccessible(true);
        HttpServer serverValue = (HttpServer) server.get(connect5Server);
        serverValue.stop(1);
        connect5Server = null;
    }

    public Connect5ServerTest(String testName) {
        super(testName);
    }

    public static Test suite() {
        return new TestSuite(Connect5ServerTest.class);
    }

    public void testConnect5Server() throws Exception {
        Field board = connect5Server.getClass().getDeclaredField("board");
        board.setAccessible(true);
        Board boardValue = (Board) board.get(connect5Server);
        Field players = connect5Server.getClass().getDeclaredField("players");
        players.setAccessible(true);
        Connect5Server.Player[] playersValue = (Connect5Server.Player[]) players.get(connect5Server);
        Field MAX_PLAYERS = connect5Server.getClass().getDeclaredField("MAX_PLAYERS");
        MAX_PLAYERS.setAccessible(true);
        int MAX_PLAYERSValue = (int) MAX_PLAYERS.get(connect5Server);
        Field currentPlayer = connect5Server.getClass().getDeclaredField("currentPlayer");
        currentPlayer.setAccessible(true);
        int currentPlayerValue = (int) currentPlayer.get(connect5Server);

        Field server = connect5Server.getClass().getDeclaredField("server");
        server.setAccessible(true);
        HttpServer serverValue = (HttpServer) server.get(connect5Server);

        assertNotNull(boardValue);
        assertNotNull(playersValue);
        assertEquals(MAX_PLAYERSValue, playersValue.length);
        assertEquals(PLAYER_X, currentPlayerValue);
        assertNotNull(serverValue);
    }

    public void testHandleDisconnection() throws Exception {
        HttpExchange he = new HttpExchangeDerived();

        Method method = connect5Server.getClass().getDeclaredMethod("handleDisconnection", HttpExchange.class);
        method.setAccessible(true);

        method.invoke(connect5Server, he);

        Field playerDisconnected = connect5Server.getClass().getDeclaredField("playerDisconnected");
        playerDisconnected.setAccessible(true);
        boolean playerDisconnectedValue = (boolean) playerDisconnected.get(connect5Server);
        assertEquals(true, playerDisconnectedValue);
    }

    public void testSendResponse() throws Exception {
        HttpExchangeDerived he = new HttpExchangeDerived();
        Method method = connect5Server.getClass().getDeclaredMethod("sendResponse", HttpExchange.class);
        method.setAccessible(true);

        method.invoke(connect5Server, he);
        assertEquals(true, he.responseHeadersSent);
        assertEquals(true, he.isClosed);
    }

    public void testHandleStartRequest() throws Exception {
        int playerNum = PLAYER_X;
        HttpExchangeDerived he = new HttpExchangeDerived();
        Method method = connect5Server.getClass().getDeclaredMethod("handleStartRequest", int.class,
                HttpExchange.class);
        method.setAccessible(true);
        Field players = connect5Server.getClass().getDeclaredField("players");
        players.setAccessible(true);
        Field playerXExchange = connect5Server.getClass().getDeclaredField("playerXExchange");
        playerXExchange.setAccessible(true);
        Field gameStarted = connect5Server.getClass().getDeclaredField("gameStarted");
        gameStarted.setAccessible(true);
        HttpExchangeDerived playerXExchangeHe = new HttpExchangeDerived();
        playerXExchange.set(connect5Server, playerXExchangeHe);

        method.invoke(connect5Server, playerNum, he);
        Connect5Server.Player[] playersValue = (Connect5Server.Player[]) players.get(connect5Server);
        assertEquals(true, he.responseHeadersSent);
        assertEquals(true, he.isClosed);
        assertNotNull(playersValue[playerNum]);
        playerNum = PLAYER_O;
        method.invoke(connect5Server, playerNum, he);
        playersValue = (Connect5Server.Player[]) players.get(connect5Server);
        boolean gameStartedValue = (boolean) gameStarted.get(connect5Server);
        assertEquals(true, he.responseHeadersSent);
        assertEquals(true, he.isClosed);
        assertEquals(true, playerXExchangeHe.responseHeadersSent);
        assertEquals(true, playerXExchangeHe.isClosed);
        assertEquals(true, gameStartedValue);
        assertNotNull(playersValue[playerNum]);
    }

    public void testHandleValidMove() throws Exception {
        HttpContextDerived context = new HttpContextDerived();
        char playerMark = Board.X_MARK;
        Board gameBoard = new Board();

        HttpExchangeDerived he = new HttpExchangeDerived();
        int row = 0;

        Field waitingPlayerExchange = connect5Server.getClass().getDeclaredField("waitingPlayerExchange");
        waitingPlayerExchange.setAccessible(true);
        HttpExchangeDerived waitingPlayerExchangeHe = new HttpExchangeDerived();
        waitingPlayerExchange.set(connect5Server, waitingPlayerExchangeHe);
        Connect5Server.Player player = connect5Server.new Player(context, playerMark, gameBoard);
        Method method = player.getClass().getDeclaredMethod("handleValidMove", HttpExchange.class, int.class);
        method.setAccessible(true);

        method.invoke(player, he, row);
        assertEquals(true, waitingPlayerExchangeHe.responseHeadersSent);
        assertEquals(true, waitingPlayerExchangeHe.isClosed);
    }

    public void testValidateAndMove() throws Exception {
        HttpContextDerived context = new HttpContextDerived();
        char playerMark = Board.X_MARK;
        Board gameBoard = new Board();

        Connect5Server.Player player = connect5Server.new Player(context, playerMark, gameBoard);
        Method method = player.getClass().getDeclaredMethod("validateAndMove", int.class);
        method.setAccessible(true);
        Field currentPlayer = connect5Server.getClass().getDeclaredField("currentPlayer");
        currentPlayer.setAccessible(true);

        int location = 0;
        int playerNum = PLAYER_X;

        int row = (int)method.invoke(player, location);
        int currentPlayerValue = (int) currentPlayer.get(connect5Server);
        assertEquals(-1, row);
        assertEquals(playerNum, currentPlayerValue);
        location = 10;
        row = (int)method.invoke(player, location);
        currentPlayerValue = (int) currentPlayer.get(connect5Server);
        assertEquals(-1, row);
        assertEquals(playerNum, currentPlayerValue);
        location = 5;
        row = (int)method.invoke(player, location);
        playerNum = currentPlayerValue;
        currentPlayerValue = (int) currentPlayer.get(connect5Server);
        assertEquals(Board.HEIGHT-1, row);
        assertEquals((playerNum+1) % 2, currentPlayerValue);
    }

    // HttpContext doesn't allow instanstation so using these inner classes
    // as dependency injection to various methods
    // Gives the advantage of customising return values and adding flags
    // to check if methods have been called
    private class HttpContextDerived extends HttpContext {

        @Override
        public Map<String, Object> getAttributes() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Authenticator getAuthenticator() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public List<Filter> getFilters() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public HttpHandler getHandler() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public String getPath() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public HttpServer getServer() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Authenticator setAuthenticator(Authenticator arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public void setHandler(HttpHandler arg0) {
            // TODO Auto-generated method stub

        }

    }

    private class HttpExchangeDerived extends HttpExchange {
        private boolean isClosed = false;
        private boolean responseHeadersSent = false;

        @Override
        public void close() {
            // TODO Auto-generated method stub
            isClosed = true;
        }

        @Override
        public Object getAttribute(String arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public HttpContext getHttpContext() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public InetSocketAddress getLocalAddress() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public HttpPrincipal getPrincipal() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public String getProtocol() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public InetSocketAddress getRemoteAddress() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public InputStream getRequestBody() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Headers getRequestHeaders() {
            // TODO Auto-generated method stub
            return new Headers();
        }

        @Override
        public String getRequestMethod() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public URI getRequestURI() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public OutputStream getResponseBody() {
            // TODO Auto-generated method stub
            return new OutputStream(){

                @Override
                public void write(int arg0) throws IOException {
                    // TODO Auto-generated method stub
                }
            };
        }

        @Override
        public int getResponseCode() {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public Headers getResponseHeaders() {
            // TODO Auto-generated method stub
            return new Headers();
        }

        @Override
        public void sendResponseHeaders(int arg0, long arg1) throws IOException {
            // TODO Auto-generated method stub
            responseHeadersSent = true;
        }

        @Override
        public void setAttribute(String arg0, Object arg1) {
            // TODO Auto-generated method stub

        }

        @Override
        public void setStreams(InputStream arg0, OutputStream arg1) {
            // TODO Auto-generated method stub

        }

    }
}