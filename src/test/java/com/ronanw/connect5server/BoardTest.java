package com.ronanw.connect5server;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.lang.reflect.*;

public class BoardTest extends TestCase {
    private final int HEIGHT = 6;
    private final int WIDTH = 9;
    private Board board;

    @Override
    protected void setUp() throws Exception {
        board = new Board();
    }

    @Override
    protected void tearDown() throws Exception {
        board = null;
    }

    public BoardTest(String testName) {
        super(testName);
    }

    public static Test suite() {
        return new TestSuite(BoardTest.class);
    }

    public void testBoard() throws Exception {
        Field field = board.getClass().getDeclaredField("board");
        field.setAccessible(true);
        char[][] value = (char[][]) field.get(board);
        assertNotNull(value);
    }

    public void testPlaceMark() throws Exception {
        int row = 5;
        int column = 3;
        char mark = 'O';

        Field field = board.getClass().getDeclaredField("board");
        field.setAccessible(true);

        assertEquals(row, board.placeMark(column, mark));
        char[][] value = (char[][]) field.get(board);
        assertEquals(value[row][column-1], mark);
        row = 4;
        mark = 'X';
        assertEquals(row, board.placeMark(column, mark));
        value = (char[][]) field.get(board);
        assertEquals(value[row][column-1], mark);

        assertEquals(-1, board.placeMark(0, mark));
        assertEquals(-1, board.placeMark(10, mark));
    }

    public void testIsEmpty() throws Exception {
        int row = 5;
        int column = 1;
        char mark = 'O';

        Method method = board.getClass().getDeclaredMethod("isEmpty", int.class, int.class);
        method.setAccessible(true);

        board.placeMark(column, mark);
        assertEquals(false, method.invoke(board, row, column-1));
        board.placeMark(column, mark);
        assertEquals(false, method.invoke(board, row-1, column-1));
    }

    public void testGetLastMarkedColumn() throws Exception {
        int column = 1;
        char mark = 'O';

        Method method = board.getClass().getDeclaredMethod("getLastMarkedColumn");
        method.setAccessible(true);

        board.placeMark(column, mark);
        assertEquals(column-1, method.invoke(board));
        column = 8;
        board.placeMark(column, mark);
        assertEquals(column-1, method.invoke(board));
    }

    public void testGetLastMarkedRow() throws Exception {
        int row = 5;
        int column = 1;
        char mark = 'O';

        Method method = board.getClass().getDeclaredMethod("getLastMarkedRow");
        method.setAccessible(true);

        board.placeMark(column, mark);
        assertEquals(row, method.invoke(board));
        board.placeMark(column, mark);
        assertEquals(row-1, method.invoke(board));
        column = WIDTH;
        board.placeMark(column, mark);
        assertEquals(row, method.invoke(board));
    }

    public void testHorizontal() throws Exception {
        char mark = 'O';
        StringBuilder sb = new StringBuilder();

        Method method = board.getClass().getDeclaredMethod("horizontal");
        method.setAccessible(true);

        for (int i = 1; i < 10; i++) {
            board.placeMark(i, mark);
            sb.append(mark);
        }
        String actual = (String)method.invoke(board);
        assertEquals(sb.toString(), actual.trim());
        mark = 'X';
        sb = new StringBuilder();
        for (int i = 2; i < WIDTH; i++) {
            board.placeMark(i, mark);
            sb.append(mark);
        }
        actual = (String)method.invoke(board);
        assertEquals(sb.toString(), actual.trim());
    }

    public void testVertical() throws Exception {
        char mark = 'O';
        int location = 1;
        StringBuilder sb = new StringBuilder();

        Method method = board.getClass().getDeclaredMethod("vertical");
        method.setAccessible(true);

        for (int i = 1; i < 7; i++) {
            board.placeMark(location, mark);
            sb.append(mark);
        }
        String actual = (String)method.invoke(board);
        assertEquals(sb.toString(), actual.trim());
        location = WIDTH;
        mark = 'X';
        sb = new StringBuilder();
        for (int i = 2; i < HEIGHT; i++) {
            board.placeMark(location, mark);
            sb.append(mark);
        }
        actual = (String)method.invoke(board);
        assertEquals(sb.toString(), actual.trim());
    }

    public void testForwardDiag() throws Exception {
        char mark = '0';
        int i = 1;
        for (int row = 0; row < HEIGHT; row++) {
            for (int column = i; column < WIDTH-3; column++) {
                mark = (column%2 == 0) ? 'X' : 'O';
                board.placeMark(column, mark);
            }
            i++;
        }

        // Resulting board will look like
        // [ ][ ][ ][ ][ ][ ][ ][ ][ ]
        // [ ][ ][ ][ ][O][ ][ ][ ][ ]
        // [ ][ ][ ][X][O][ ][ ][ ][ ]
        // [ ][ ][O][X][O][ ][ ][ ][ ]
        // [ ][X][O][X][O][ ][ ][ ][ ]
        // [O][X][O][X][O][ ][ ][ ][ ]

        Method method = board.getClass().getDeclaredMethod("forwardDiag");
        method.setAccessible(true);
        String actual = (String)method.invoke(board);

        String expected = "OXOXO";
        assertEquals(expected.trim(), actual.trim());
    }

    public void testBackwardDiag() throws Exception {
        char mark = '0';
        int i = HEIGHT;
        for (int row = 0; row < HEIGHT-1; row++) {
            for (int column = 0; column < i; column++) {
                mark = (column%2 == 0) ? 'X' : 'O';
                board.placeMark(column, mark);
            }
            i--;
        }

        // Resulting board will look like
        // [ ][ ][ ][ ][ ][ ][ ][ ][ ]
        // [O][ ][ ][ ][ ][ ][ ][ ][ ]
        // [O][X][ ][ ][ ][ ][ ][ ][ ]
        // [O][X][O][ ][ ][ ][ ][ ][ ]
        // [O][X][O][X][ ][ ][ ][ ][ ]
        // [O][X][O][X][O][ ][ ][ ][ ]

        Method method = board.getClass().getDeclaredMethod("backwardDiag");
        method.setAccessible(true);
        String actual = (String)method.invoke(board);

        String expected = "OXOXO";
        assertEquals(expected.trim(), actual.trim());
    }

    public void testIsGameOver() throws Exception {
        char mark = Board.X_MARK;
        Field field = board.getClass().getDeclaredField("board");
        field.setAccessible(true);
        Method isGameOverMethod = board.getClass().getDeclaredMethod("isGameOver");
        isGameOverMethod.setAccessible(true);

        Method resetBoardMethod = board.getClass().getDeclaredMethod("resetBoard");
        resetBoardMethod.setAccessible(true);

        assertEquals(false, isGameOverMethod.invoke(board));

        // horizontal
        for (int i = 1; i < 6; i++) {
            board.placeMark(i, mark);
        }
        assertEquals(true, isGameOverMethod.invoke(board));
        resetBoardMethod.invoke(board);
        // vertical
        mark = Board.O_MARK;
        int column = 3;
        for (int i = 1; i < 6; i++) {
            board.placeMark(column, mark);
        }
        assertEquals(true, isGameOverMethod.invoke(board));
        resetBoardMethod.invoke(board);
        // forward diagonal
        int i = 1;
        for (int row = 0; row < HEIGHT; row++) {
            for (column = i; column < WIDTH-3; column++) {
                if (column == i) {
                    mark = Board.X_MARK;
                } else {
                    mark = (column%2 == 0) ? Board.X_MARK : Board.O_MARK;
                }
                board.placeMark(column, mark);
            }
            i++;
        }
        // board now looks like:
        // [ ][ ][ ][ ][ ][ ][ ][ ][ ]
        // [ ][ ][ ][ ][X][ ][ ][ ][ ]
        // [ ][ ][ ][X][O][ ][ ][ ][ ]
        // [ ][ ][X][X][O][ ][ ][ ][ ]
        // [ ][X][O][X][O][ ][ ][ ][ ]
        // [X][X][O][X][O][ ][ ][ ][ ]
        assertEquals(true, isGameOverMethod.invoke(board));
        resetBoardMethod.invoke(board);
        // backward diagonal
        i = HEIGHT;
        for (int row = 0; row < HEIGHT-1; row++) {
            for (column = 0; column < i; column++) {
                if (column == i-1) {
                    mark = Board.O_MARK;
                } else {
                    mark = (column%2 == 0) ? 'X' : 'O';
                }
                board.placeMark(column, mark);
            }
            i--;
        }
        // board now looks like:
        // [ ][ ][ ][ ][ ][ ][ ][ ][ ]
        // [O][ ][ ][ ][ ][ ][ ][ ][ ]
        // [O][O][ ][ ][ ][ ][ ][ ][ ]
        // [O][X][O][ ][ ][ ][ ][ ][ ]
        // [O][X][O][O][ ][ ][ ][ ][ ]
        // [O][X][O][X][O][ ][ ][ ][ ]
        assertEquals(true, isGameOverMethod.invoke(board));
        resetBoardMethod.invoke(board);
    }
}
