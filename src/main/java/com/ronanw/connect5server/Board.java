package com.ronanw.connect5server;

public class Board {
    public static final char X_MARK = 'X', O_MARK = 'O';
    private char[][] board;
    public static final int HEIGHT = 6;
    public static final int WIDTH = 9;
    private int lastMarkedRow;
    private int lastMarkedColumn;

    public Board() {
        resetBoard();
    }

    private void resetBoard() {
        board = new char[HEIGHT][WIDTH];
    }

    public int placeMark(int location, char mark) {
        if (location < 1 || location > 9) {
            return -1;
        }
        boolean result = false;
        int row;
        for (row = board.length - 1; row >= 0; row--) {
            if (isEmpty(row, location - 1)) {
                board[row][location - 1] = mark;
                lastMarkedRow = row;
                lastMarkedColumn = location - 1;
                result = true;
                break;
            }
        }

        return row;
    }

    private boolean isEmpty(int row, int column) {
        return !(board[row][column] == X_MARK || board[row][column] == O_MARK);
    }

    public int getLastMarkedColumn() {
        return lastMarkedColumn;
    }

    public int getLastMarkedRow() {
        return lastMarkedRow;
    }

    private String horizontal() {
        return new String(board[lastMarkedRow]);
    }

    private String vertical() {
        StringBuilder sb = new StringBuilder(HEIGHT);

        for (int row = 0; row < HEIGHT; row++) {
            sb.append(board[row][lastMarkedColumn]);
        }

        return sb.toString();
    }

    private String forwardDiag() {
        StringBuilder sb = new StringBuilder(HEIGHT);

        for (int row = 0; row < HEIGHT; row++) {
            int column = lastMarkedColumn + lastMarkedRow - row;
            if (0 <= column && column < WIDTH) {
                sb.append(board[row][column]);
            }
        }

        return sb.toString();
    }

    private String backwardDiag() {
        StringBuilder sb = new StringBuilder(HEIGHT);

        for (int row = 0; row < HEIGHT; row++) {
            int column = lastMarkedColumn - lastMarkedRow + row;
            if (0 <= column && column < WIDTH) {
                sb.append(board[row][column]);
            }
        }

        return sb.toString();
    }

    public boolean isGameOver() {
        char mark = board[lastMarkedRow][lastMarkedColumn];
        if (mark != X_MARK && mark != O_MARK) {
            return false;
        }
        StringBuilder sb = new StringBuilder();
        final int FULL_LINE = 5;
        for (int i = 0; i < FULL_LINE; i++) {
            sb.append(mark);
        }

        return horizontal().contains(sb.toString()) ||
               vertical().contains(sb.toString()) ||
               forwardDiag().contains(sb.toString()) ||
               backwardDiag().contains(sb.toString());
    }
}