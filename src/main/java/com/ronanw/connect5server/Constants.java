package com.ronanw.connect5server;

public final class Constants {
    private Constants() {
    }

    public static final String PAYLOAD = "";

    public static final String PLAYER_MARK = "player_mark";
    public static final String PLAYER_NAME = "player_name";
    public static final String PLAYER_COLOUR = "player_colour";
    public static final String OPPONENT_NAME = "opponent_name";
    public static final String OPPONENT_COLOUR = "opponent_colour";
    public static final String WAITING_FOR_OPPONENT = "waiting_for_opponent";
    public static final String WAITING_FOR_TURN = "waiting_for_turn";
    public static final String OPPONENT_MOVE = "opponent_move";
    public static final String REQUEST_TO_START = "request_to_start";
    public static final String OTHER_PLAYER_HAS_JOINED = "other_player_has_joined";
    public static final String LOCATION = "location";
    public static final String VALID_MOVE = "valid_move";
    public static final String INVALID_MOVE = "invalid_move";
    public static final String LAST_MARKED_ROW = "last_marked_row";
    public static final String LAST_MARKED_COLUMN = "last_marked_column";
    public static final String INFORM_WIN = "inform_win";
    public static final String INFORM_OPPONENT_WIN = "inform_opponent_win";
    public static final String NOTIFY_DISCONNECTION = "notify_disconnection";
    public static final String GAME_IN_PROGRESS = "game_in_progress";
}