package com.ronanw.connect5server;

import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;
import com.sun.net.httpserver.Headers;
import java.net.InetSocketAddress;
import java.io.IOException;
import java.io.OutputStream;

public class Connect5Server {
    private Board board;
    private Player[] players;
    private HttpServer server;
    private HttpExchange playerXExchange;
    private HttpExchange playerOExchange;
    private HttpExchange waitingPlayerExchange;
    private String playerXName;
    private String playerOName;
    private String playerXColour;
    private String playerOColour;
    private int currentPlayer;
    private boolean gameStarted = false;
    private boolean playerDisconnected = false;
    private boolean isUnitTest = false;
    private final int PORT = 8080;
    private final int MAX_PLAYERS = 2;

    public Connect5Server(boolean isUnitTest) {
        // The unit test flag is used to prevent System.exit() being called in the case
        // of exceptions in unit tests
        this.isUnitTest = isUnitTest;
        startServer();
    }

    public Connect5Server() {
        startServer();
    }

    private void startServer() {
        board = new Board();
        players = new Player[MAX_PLAYERS];
        currentPlayer = Player.PLAYER_X;

        try {
            InetSocketAddress address = new InetSocketAddress("localhost", PORT);
            server = HttpServer.create(address, MAX_PLAYERS);
            server.start();
        } catch (IOException ioException) {
            System.out.println("Server failed to start");
            ioException.printStackTrace();
            if (!isUnitTest) {
                System.exit(1);
            }
        }
    }

    public void execute() {
        var playerNum = new Object() {
            int ordinal = 0;
        };
        // The server processes non-game logic requests on the /connect5 endpoint
        // The communication between the clients and the server is carried out through
        // the key value headers of GET requests.
        // All header keys are defined in the Constants class
        HttpContext mainConext = server.createContext("/connect5");
        mainConext.setHandler((he) -> {
            System.out.println("Server received a request");
            Headers requestHeaders = he.getRequestHeaders();
            requestHeaders.forEach((k, v) -> {
                String value = v.get(0);
                if (k.equalsIgnoreCase(Constants.REQUEST_TO_START)) {
                    System.out.println(Constants.REQUEST_TO_START);
                    handleStartRequest(playerNum.ordinal++, he);
                } else if (k.equalsIgnoreCase(Constants.WAITING_FOR_OPPONENT)) {
                    System.out.println(Constants.WAITING_FOR_OPPONENT);
                    playerXExchange = he;
                    waitingPlayerExchange = he;
                } else if (k.equalsIgnoreCase(Constants.WAITING_FOR_TURN)) {
                    System.out.println(Constants.WAITING_FOR_TURN);
                    if (value.charAt(0) == Board.X_MARK) {
                        waitingPlayerExchange = he;
                        playerXExchange = he;
                    } else {
                        waitingPlayerExchange = he;
                        playerOExchange = he;
                    }
                } else if (requestHeaders.containsKey(Constants.NOTIFY_DISCONNECTION)) {
                    handleDisconnection(he);
                }
            });
        });
    }

    private void handleDisconnection(HttpExchange he) {
        // When the server is notified of a client's disconnection,
        // it notifies the other client and exits
        Headers requestHeaders = he.getRequestHeaders();
        playerDisconnected = true;
        try {
            if (gameStarted) {
                char m = requestHeaders.getFirst(Constants.NOTIFY_DISCONNECTION).charAt(0);
                HttpExchange exchange = m == Board.X_MARK ? playerOExchange : playerXExchange;
                Headers responseHeaders = exchange.getResponseHeaders();
                responseHeaders.add(Constants.NOTIFY_DISCONNECTION, "");
                sendResponse(exchange);
            }

            sendResponse(he);
        } catch (Exception ee) {
            if (!isUnitTest) {
                System.exit(1);
            }
        }
    }

    private void sendResponse(HttpExchange exchange) throws IOException {
        exchange.sendResponseHeaders(200, Constants.PAYLOAD.getBytes().length);
        OutputStream output = exchange.getResponseBody();
        output.write(Constants.PAYLOAD.getBytes());
        output.flush();
        exchange.close();
    }

    private void notifyPlayerX() {
        try {
            Headers responseHeaders = playerXExchange.getResponseHeaders();
            responseHeaders.add(Constants.OPPONENT_NAME, playerOName);
            responseHeaders.add(Constants.OPPONENT_COLOUR, playerOColour);
            sendResponse(playerXExchange);
        } catch (Exception ee) {
            System.err.println("Received error notifying player X");
            ee.printStackTrace();
            if (!isUnitTest) {
                System.exit(1);
            }
        }
    }

    private void handleStartRequest(int playerNum, HttpExchange he) {
        try {
            if (playerNum > Player.PLAYER_O) {
                Headers responseHeaders = he.getResponseHeaders();
                responseHeaders.add(Constants.GAME_IN_PROGRESS, "");
            } else {
                handlePlayerStart(playerNum, he);
            }
            sendResponse(he);
        } catch (Exception ee) {
            System.out.println("Failed to handle player start request");
            ee.printStackTrace();
            if (!isUnitTest) {
                System.exit(1);
            }
        }
    }

    private void handlePlayerStart(int playerNum, HttpExchange he) throws IOException {
        char mark = (playerNum == Player.PLAYER_X ? Board.X_MARK : Board.O_MARK);
        Headers requestHeaders = he.getRequestHeaders();
        requestHeaders.forEach((k, v) -> {
            if (k.equalsIgnoreCase(Constants.PLAYER_NAME)) {
                if (playerNum == Player.PLAYER_O) {
                    playerOName = v.get(0);
                } else if (playerNum == Player.PLAYER_X) {
                    playerXName = v.get(0);
                }
            }
            if (k.equalsIgnoreCase(Constants.PLAYER_COLOUR)) {
                if (playerNum == Player.PLAYER_O) {
                    playerOColour = v.get(0);
                } else if (playerNum == Player.PLAYER_X) {
                    playerXColour = v.get(0);
                }
            }
        });
        if (playerNum == Player.PLAYER_O) {
            notifyPlayerX();
            gameStarted = true;
        }
        HttpContext context = server.createContext("/player" + mark);
        players[playerNum] = new Player(context, mark, board);
        players[playerNum].start();
        Headers responseHeaders = he.getResponseHeaders();
        responseHeaders.add(Constants.PLAYER_MARK, mark + "");
        if (playerNum == Player.PLAYER_O) {
            responseHeaders.add(Constants.OPPONENT_NAME, playerXName);
            responseHeaders.add(Constants.OPPONENT_COLOUR, playerXColour);
        }
    }

    public static void main(String args[]) {
        Connect5Server application = new Connect5Server();
        application.execute();
    }

    public class Player extends Thread {
        public static final int PLAYER_X = 0, PLAYER_O = 1;
        private HttpContext playerContext;
        private char mark;
        private Board board;
        protected boolean suspended = true;

        public Player(HttpContext context, char playerMark, Board gameBoard) throws IOException {
            mark = playerMark;
            board = gameBoard;
            playerContext = context;
        }

        public void run() {
            runGame();
        }

        public void runGame() {
            // The game logic is carried out between the /player+mark endpoint of the server
            // Each client communicates with a player endpoint to send a location and have it validated
            playerContext.setHandler((he) -> {
                System.out.println("Player" + mark + " received a request");
                Headers requestHeaders = he.getRequestHeaders();
                if (requestHeaders.containsKey(Constants.LOCATION)) {
                    System.out.println("LOCATION RECEIVED");
                    String value = requestHeaders.getFirst(Constants.LOCATION);
                    int location = Integer.parseInt(value);
                    int row;
                    if (playerDisconnected) {
                        Headers responseHeaders = he.getResponseHeaders();
                        responseHeaders.add(Constants.NOTIFY_DISCONNECTION, "");
                    } else if ((row = validateAndMove(location)) != -1) {
                        handleValidMove(he, row);
                    } else {
                        Headers responseHeaders = he.getResponseHeaders();
                        responseHeaders.add(Constants.INVALID_MOVE, "");
                    }
                    sendResponse(he);
                }
            });
        }

        private void handleValidMove(HttpExchange he, int row) {
            Headers responseHeaders = he.getResponseHeaders();
            responseHeaders.add(Constants.VALID_MOVE, "" + row);

            // When a game is in play there is always a player waiting for a turn (Async Http request)
            // They are informed when a move is made and they update their board accordingly and make their move
            Headers waitingPlayerResponseHeaders = waitingPlayerExchange.getResponseHeaders();

            waitingPlayerResponseHeaders.add(Constants.OPPONENT_MOVE,
                    board.getLastMarkedRow() + " " + board.getLastMarkedColumn());
            boolean gameOver = board.isGameOver();
            if (gameOver) {
                System.out.println("GAME OVER");
                responseHeaders.add(Constants.INFORM_WIN, "");
                waitingPlayerResponseHeaders.add(Constants.INFORM_OPPONENT_WIN, "");
            }
            try {
                sendResponse(waitingPlayerExchange);
            } catch (Exception ee) {
                System.out.println("Error sending response for move");
                ee.printStackTrace();
                if (!isUnitTest) {
                    System.exit(1);
                }
            }
        }

        public int validateAndMove(int location) {
            int row = board.placeMark(location, currentPlayer == Player.PLAYER_X ? Board.X_MARK : Board.O_MARK);

            if (row != -1) {
                // Current player is updated only if the move is valid
                currentPlayer = (currentPlayer + 1) % 2;
            }

            return row;
        }
    }
}